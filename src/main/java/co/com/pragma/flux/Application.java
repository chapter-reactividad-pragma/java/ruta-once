package co.com.pragma.flux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        demonstrateMapWithFluxOfStrings();
        demonstrateFlatMapWithSingleValueFlux();
        demonstrateFlatMapWithMultipleFluxes();
    }

    private static void demonstrateMapWithFluxOfStrings() {
        Flux<String> fluxOfStringNumbers = Flux.just("1", "2", "3");
        Flux<Integer> fluxOfIntegers = fluxOfStringNumbers.map(Integer::parseInt);
        subscribeToFlux(fluxOfIntegers, "demostrateMapWithFluxOfStrings");
    }

    private static void demonstrateFlatMapWithSingleValueFlux() {
        Flux<String> fluxOfStringNumbers = Flux.just("4", "5", "6");
        Flux<Integer> fluxOfIntegers = fluxOfStringNumbers.flatMap(s -> Flux.just(Integer.parseInt(s)));
        subscribeToFlux(fluxOfIntegers, "demostrateFlatMapWithSingleValueFlux");
    }

    private static void demonstrateFlatMapWithMultipleFluxes() {
        Flux<String> fluxOfStringNumbersOne = Flux.just("7", "8", "9");
        Flux<String> fluxOfStringNumbersTwo = Flux.just("10", "11", "12");
        Flux<Integer> fluxOfIntegers = Flux.concat(fluxOfStringNumbersOne, fluxOfStringNumbersTwo).flatMap(s -> Flux.just(Integer.parseInt(s)));
        subscribeToFlux(fluxOfIntegers, "demostrateConcatAndFlatMap");
    }

    private static void subscribeToFlux(Flux<Integer> intFlux, String demoMethod) {
        System.out.println("\nStarting " + demoMethod + ":");
        intFlux.subscribe(
                number -> System.out.println("Next number: " + number),
                error -> System.out.println("Error: " + error),
                () -> System.out.println("End of flow for " + demoMethod)
        );
    }
}
