# Ruta webflux 11

Hoy vamos a llevar los conceptos y/o métodos aprendidos
y lo pondremos en marcha desde la forma básica hasta llevarlo a un siguiente nivel.
Recordemos que lo vamos a hacer sobre SpringBoot(WebFlux)

### Reto

Transforma un flujo de string a enteros

Pasemos por cada componente del código:

### Explicación paso a paso

Paso 1: Importar las librerías necesarias

```java
import reactor.core.publisher.Flux;
```

Paso 2: El método main es el punto de entrada del programa.
Efectúa una serie de llamadas a métodos para demostrar ciertos conceptos de programación reactiva con Flux.

````java
public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
    demonstrateMapWithFluxOfStrings();
    demonstrateFlatMapWithSingleValueFlux();
    demonstrateFlatMapWithMultipleFluxes();
}
````

Paso 3: El método demonstrateMapWithFluxOfStrings crea un Flux de cadenas de texto,
que luego son transformadas en un Flux de enteros por medio del método map.
Luego se suscribe a este Flux con el método subscribeToFlux.

````java
private static void demonstrateMapWithFluxOfStrings() {
    Flux<String> fluxOfStringNumbers = Flux.just("1", "2", "3");
    Flux<Integer> fluxOfIntegers = fluxOfStringNumbers.map(Integer::parseInt);
    subscribeToFlux(fluxOfIntegers, "demostrateMapWithFluxOfStrings");
}
````

Paso 4: El método demonstrateFlatMapWithSingleValueFlux hace algo similar, pero en lugar de map usa flatMap, y permite la transformación a un nuevo Flux.

````java
private static void demonstrateFlatMapWithSingleValueFlux() {
    Flux<String> fluxOfStringNumbers = Flux.just("4", "5", "6");
    Flux<Integer> fluxOfIntegers = fluxOfStringNumbers.flatMap(s -> Flux.just(Integer.parseInt(s)));
    subscribeToFlux(fluxOfIntegers, "demostrateFlatMapWithSingleValueFlux");
}
````

Paso 5: El método demonstrateFlatMapWithMultipleFluxes concatena dos Flux de cadenas de texto y luego los transforma en un Flux de enteros.

````java
private static void demonstrateFlatMapWithMultipleFluxes() {
    Flux<String> fluxOfStringNumbersOne = Flux.just("7", "8", "9");
    Flux<String> fluxOfStringNumbersTwo = Flux.just("10", "11", "12");
    Flux<Integer> fluxOfIntegers = Flux.concat(fluxOfStringNumbersOne, fluxOfStringNumbersTwo).flatMap(s -> Flux.just(Integer.parseInt(s)));
    subscribeToFlux(fluxOfIntegers, "demostrateConcatAndFlatMap");
}
````

Paso 6: Eventualmente, todos estos métodos terminan llamando al método subscribeToFlux.
El método subscribeToFlux imprime un mensaje cuando comienza el proceso,
registra una Consumer para manejar cada número emitido por el Flux,
registra una Consumer para manejar cualquier error,
y registra un Runnable que se ejecutará cuando se complete el Flux.

````java
private static void subscribeToFlux(Flux<Integer> intFlux, String demoMethod) {
    System.out.println("\nStarting " + demoMethod + ":");
    intFlux.subscribe(number -> System.out.println("Next number: " + number), error -> System.err.println("Error: " + error), () -> System.out.println("End of flow for " + demoMethod));
}
````

Paso 7: el resultado final nos queda de esta manera:

````
Starting demostrateMapWithFluxOfStrings:
Next number: 1
Next number: 2
Next number: 3
End of flow for demostrateMapWithFluxOfStrings

Starting demostrateFlatMapWithSingleValueFlux:
Next number: 4
Next number: 5
Next number: 6
End of flow for demostrateFlatMapWithSingleValueFlux

Starting demostrateConcatAndFlatMap:
Next number: 7
Next number: 8
Next number: 9
Next number: 10
Next number: 11
Next number: 12
End of flow for demostrateConcatAndFlatMap
````

Paso 8: Asi que llego el momento que tu lo hagas, es tu turno, tu puedes!
